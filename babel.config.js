module.exports = {
	plugins: ["array-includes"],
	presets: [[require("@babel/preset-env"), { useBuiltIns: "entry", corejs: 3, bugfixes: true }]],
};
