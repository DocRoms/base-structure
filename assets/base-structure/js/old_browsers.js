// import * as params from "@params";
import cssVars from "css-vars-ponyfill/dist/css-vars-ponyfill.esm.min.js";
//import svg4everybody from "svg4everybody/dist/svg4everybody.min.js";

// We need this ponyfill to transform calc() in dynamically added styles (e.g. Web Components)
// Note: PostCSS already handle regular static styles
// See https://jhildenbiddle.github.io/css-vars-ponyfill/
cssVars({
	shadowDOM: true,

	// Sources
	include: "link[rel=stylesheet],style",
	exclude: "",
	variables: {},

	// Options
	onlyLegacy: true,
	preserveStatic: true,
	preserveVars: false,
});

//svg4everybody();

// /**
//  * Load a custom element definitions in `waitFor` and return a promise
//  * WARNING: at the moment ESBuild does not support transpiling "const" so we use var
//  */
// function loadScript(src) {
// 	return new Promise(function (resolve, reject) {
// 		var script = document.createElement("script");
// 		script.src = src;
// 		script.onload = resolve;
// 		script.onerror = reject;
// 		document.head.appendChild(script);
// 	});
// }

// WebComponents.waitFor(() => {
// 	// At this point we are guaranteed that all required polyfills have
// 	// loaded, and can use web components APIs.
// 	// Next, load element definitions that call `customElements.define`.
// 	// Note: returning a promise causes the custom elements
// 	// polyfill to wait until all definitions are loaded and then upgrade
// 	// the document in one batch, for better performance.
// 	return loadScript(params.componentsScript);
// });
