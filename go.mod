module gitlab.com/goodimpact/goodimpact-hugo/modules/base-structure

go 1.15

require (
	github.com/hankchizljaw/modern-css-reset v0.0.0-20210118105237-e7500a6c7fa3 // indirect
	gitlab.com/goodimpact/every-layout-css v0.0.0-20211221221050-b7779e404e38 // indirect
	gitlab.com/goodimpact/every-layout-wc v0.0.0-20210715225714-a9629e5e8f4a // indirect
	gitlab.com/neotericdesign-tools/hugo-content-module-style-guides.git v0.0.0-20200513165827-e43fa80f0593 // indirect
)
