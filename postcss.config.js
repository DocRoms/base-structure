const autoprefixer = require("autoprefixer");
const presetEnv = require("postcss-preset-env")({
	stage: 0,
	useBuiltIns: "usage",
});
const calc = require("postcss-calc")({});
const purgecss = require("@fullhuman/postcss-purgecss")({
	content: ["./hugo_stats.json"],
	defaultExtractor: (content) => {
		let els = JSON.parse(content).htmlElements;
		return els.tags.concat(els.classes, els.ids);
	},
	variables: true,
	safelist: {
		standard: [
			"::placeholder",
			"class",
			"href",
			"js-ok",
			/:focus/,
			/tabindex/,
			/aria/,
			/:defined/,
			/wc/,
			/h\d/,
			/data-/,
			/type/, // [type='text'], [type='checkbox'], etc..
		],
	},
	defaultExtractor: (content) => {
		let els = JSON.parse(content).htmlElements;
		return els.tags.concat(els.classes, els.ids);
	},
});

module.exports = {
	plugins: [autoprefixer, ...(process.env.HUGO_ENVIRONMENT === "production" ? [presetEnv, calc, purgecss] : [])],
};
